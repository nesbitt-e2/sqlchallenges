DROP PROCEDURE IF EXISTS q1;

DELIMITER //
create procedure q1 (IN tickerin text, IN indate datetime) 
BEGIN
	select ticker as Ticker, `close` as Close_Price, vol as Volume, 
    date_format(`date`, '%Y-%m-%d') as `Date`, date_format(`date`, '%H:%i') as Start_Time,
	date_format(date_add(indate, interval 5 hour), '%H:%i') as EndTime,
	SUM(vol*close)/SUM(vol) as VOLUME_WEIGHTED_PRICE
    from trade_data
    where `date` between indate and DATE_ADD(indate, interval 5 hour)
	and ticker = tickerin;

END //
DELIMITER ;