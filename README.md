# SQLChallenges
### Challenge One
I first created a database using the command 'create database challengeone'.
Prior to importing the data I opened it in notepad and adjusted the column names to avoid keyword issues within mysql. I imported the data set using the Table Import Data Wizard, for this I had to select the .csv file and then once it was read in I had to adjust the date column as by default it was set to bigint as a data type. This was changed to datetime and the import was finished.
I then created the stored procedure script which can be seen as sqlchallenge1.sql
This can be called using the script sqlchallenge1call.sql 

### Challenge Two
I first imported the data using the table import wizard, this involved having to change the date data type to datetime and set the date format to be %m/%d/%Y as this is how it was formated in the csv file.
I then created the stored procedure script which can be seen as sqlchallenge2.sql
This can be called using the script sqlchallenge1call2.sql 