DROP PROCEDURE IF EXISTS q2;

DELIMITER //
create procedure q2()

begin

create temporary table days_with_biggest_range
select `Date`, `open`, `close`, ABS(`open` - `close`) AS `Range`
from trade_data
order by `Range` desc
limit 4;

create temporary table max_price
select `Date`, max(high) as HighPrice
from trade_data
where `Date` in 
	(
		select `Date`
		from days_with_biggest_range
     )
group by `Date`;

create temporary table time_at_max_price
select trade_data.`Date`, `Time`
from trade_data 
inner join max_price 
on trade_data.`Date` = max_price.`Date` 
and trade_data.`High` = max_price.`HighPrice`
where trade_data.`Date` in 
	(
		select `Date`
		from days_with_biggest_range 
     );

create temporary table date_time_range
select distinct time_at_max_price.`Date`, `Range`, `Time`
from days_with_biggest_range
inner join time_at_max_price
on days_with_biggest_range.`Date` = time_at_max_price.`Date`;

select date_format(`Date`, "%m/%d/%Y") as `Date`, `Range`, time_format(`Time`, "%i:%s") as `Time`
from date_time_range;

drop temporary table if exists days_with_biggest_range;
drop temporary table if exists max_price;
drop temporary table if exists time_at_max_price;
drop temporary table if exists date_time_range;

end //
DELIMITER ;

